//
//  ViewController.swift
//  ProyectoCoredATA
//
//  Created by Kevin on 23/1/18.
//  Copyright © 2018 Kevin. All rights reserved.
//

import UIKit
import CoreData //importar coredata

class ViewController: UIViewController {

    @IBOutlet weak var inputName: UITextField!
    @IBOutlet weak var inputAddress: UITextField!
    @IBOutlet weak var inputPhone: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func savePerson() {
        let entityDescripcion = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let person = Person(entity: entityDescripcion!, insertInto: manageObjectContext)
        
        person.address = inputAddress.text ?? ""
        person.name = inputName.text ?? ""
        person.phone = inputPhone.text ?? ""
        
        
        do{
            try manageObjectContext.save()
            clearTextField()
        } catch {
            print("error")
        }
    }
    
    func clearTextField(){
        inputAddress.text = ""
        inputName.text = ""
        inputPhone.text = ""
        
    }
    
    @IBAction func btnSave(_ sender: Any) {
        savePerson()
    }
    
    
    
    
    
    
    func findAll(){
        //let entityDescripcion = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        do{
            var results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            for result in results {
                
                let person = result as! Person
                
                print("Name: \(person.name ?? "") ", terminator: "")
                print("Address: \(person.address ?? "") ", terminator: "")
                print("Phone: \(person.phone ?? "") ", terminator: "")
                print()
            }
            
        }catch{
            print("Error :v finding people")
        }
    }
    
    func findOne(){
        let entityDescripcion = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        request.entity = entityDescripcion
        
        let predicate = NSPredicate(format: "name = %@", inputName.text!)
        
        request.predicate = predicate
        do{
            var results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            if results.count > 0 {
                let match = results[0] as! Person
                inputAddress.text = match.address
                inputName.text = match.name
                inputPhone.text = match.phone
            }else{
                inputAddress.text = "n/a"
                inputName.text = "n/a"
                inputPhone.text = "n/a"
            }
            
//            for result in results {
//
//                let person = result as! Person
//
//                print("Name: \(person.name ?? "") ", terminator: "")
//                print("Address: \(person.address ?? "") ", terminator: "")
//                print("Phone: \(person.phone ?? "") ", terminator: "")
//                print()
//            }
            
        }catch{
            print("Error :v finding people")
        }
        
    }
    
    @IBAction func btnFind(_ sender: Any) {
        if inputName.text == "" {
            findAll()
            return
        }
        findOne()
        
    }
    
}

